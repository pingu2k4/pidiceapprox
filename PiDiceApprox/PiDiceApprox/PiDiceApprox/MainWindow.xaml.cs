﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using PiDiceApprox.ViewModels;

namespace PiDiceApprox
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = MWVM;
        }

        private MainWindowViewModel MWVM = new MainWindowViewModel();

        private void OnClose(object sender, System.ComponentModel.CancelEventArgs e)
        {
            MWVM.WindowClosing(e);
        }

        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            //this.WindowState = WindowState.Maximized;
        }
    }
}
