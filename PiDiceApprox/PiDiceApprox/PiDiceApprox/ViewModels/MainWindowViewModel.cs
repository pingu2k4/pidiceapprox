﻿using log4net;
using log4net.Config;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using PiDiceApprox.Commands;
using PiDiceApprox.Models;
using PiDiceApprox.Views;
using System.Threading;
using System.Timers;
using IntXLib;

namespace PiDiceApprox.ViewModels
{
    class MainWindowViewModel : INotifyPropertyChanged
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public MainWindowViewModel()
        {
            SettingsUpgrade();
            SetBGOpacity();
            CheckParameters();

            BGWorker = new BackgroundWorker();
            BGWorker.WorkerReportsProgress = true;
            BGWorker.WorkerSupportsCancellation = true;
            BGWorker.DoWork += BGWorker_DoWork;
            BGWorker.ProgressChanged += BGWorker_ProgressChanged;
            BGWorker.RunWorkerCompleted += BGWorker_RunWorkerCompleted;

            BGLoader = new BackgroundWorker();
            BGLoader.WorkerReportsProgress = true;
            BGLoader.WorkerSupportsCancellation = true;
            BGLoader.DoWork += BGLoader_DoWork;
            BGLoader.ProgressChanged += BGLoader_ProgressChanged;
            BGLoader.RunWorkerCompleted += BGLoader_RunWorkerCompleted;

            BGTimer = new System.Timers.Timer(100);
            BGTimer.AutoReset = true;

            NSides = Properties.Settings.Default.NSides;
            NTrials = Properties.Settings.Default.NTrials;
            NUpdate = Properties.Settings.Default.NUpdate;
            NDice = Properties.Settings.Default.NDice;
            NError = Properties.Settings.Default.NError;
        }

        #region BOOT
        private void SettingsUpgrade()
        {
            if (Properties.Settings.Default.NeedUpgrade)
            {
                Properties.Settings.Default.Upgrade();
                Properties.Settings.Default.NeedUpgrade = false;

                Properties.Settings.Default.Save();
            }
        }

        private void CheckParameters()
        {
            if (Application.Current.Properties["FileName"] != null)
            {
                //We opened up with parameters - Handle this here.
            }
        }
        #endregion

        #region BGWORKERS
        #region BGWORKER
        private void BGWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Status1 = "Finished Rolling.";
            double AveragePi = L.Average(item => item.Pi);
            double AverageError = Math.Abs(Math.PI - AveragePi) / Math.PI * 100;
            double WorstPi = L.First(Item => Item.Error == L.Max(item => item.Error)).Pi;
            double WorstError = Math.Abs(Math.PI - WorstPi) / Math.PI * 100;
            double BestPi = L.First(Item => Item.Error == L.Min(item => item.Error)).Pi;
            double BestError = Math.Abs(Math.PI - BestPi) / Math.PI * 100;

            Status2 = "Average value for pi: " + AveragePi + ".\n"
                 + "Average Error: " + Math.Round(AverageError,4) + "%.\n"
                 + "Worst value for pi: " + WorstPi + ".\n"
                 + "Worst Error: " + Math.Round(WorstError, 4) + "%.\n"
                 + "Best value for pi: " + BestPi + ".\n"
                 + "Best Error: " + Math.Round(BestError, 4) + "%.\n";
            Stopped = true;
            Running = false;
        }

        private void BGWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            ulong Coprimes = (e.UserState as Tuple<ulong, ulong>).Item1;
            ulong Cofactors = (e.UserState as Tuple<ulong, ulong>).Item2;
            double Pi = Math.Sqrt(6d / ((double)Coprimes / ((double)Coprimes + (double)Cofactors)));
            double Error = Math.Abs(Math.PI - Pi) / Math.PI * 100;
            Status2 = "Coprimes: " + Coprimes.ToString("N0") + " / " + (Coprimes + Cofactors).ToString("N0") + ".\n"
                + "Pi Estimate: " + Pi + ".\n"
                + "Percentage Error: " + Math.Round(Error, 2) + "%.";
        }

        private void BGWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker Worker = sender as BackgroundWorker;

            Status1 = "Rolling dice...";
            ulong i = 0;
            ulong j = 0;
            Coprimes = 0;
            Cofactors = 0;
            Random Rand = new Random();
            L = new List<TrialRun>();
            while (true)
            {
                if (Worker.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }

                //Logic goes here.
                ulong X = (ulong)Math.Ceiling(Rand.NextDouble() * NSides);
                ulong Y = (ulong)Math.Ceiling(Rand.NextDouble() * NSides);

                ulong GCD = MathHelper.GCD(X, Y);

                if(GCD == 1)
                {
                    Coprimes++;
                }
                else
                {
                    Cofactors++;
                }

                if(Rand.NextDouble() * 100 <= NError)
                {
                    if(GCD == 1)
                    {
                        Coprimes--;
                        Cofactors++;
                    }
                    else
                    {
                        Cofactors--;
                        Coprimes++;
                    }
                }
                
                j++;

                if(j >= NDice)
                {
                    Worker.ReportProgress(1, new Tuple<ulong, ulong>(Coprimes, Cofactors));
                    i++;
                    L.Add(new TrialRun(Coprimes, Cofactors));
                    Coprimes = 0;
                    Cofactors = 0;
                    j = 0;
                }

                if(i >= NTrials)
                {
                    e.Cancel = true;
                    break;
                }

                //This breaks before saving to file. If you have long lasting loops for the logic, include a copy of this breakout in those also.
                if (Worker.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }
            }
        }
        #endregion
        #region BGLOADER
        private void BGLoader_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            PostLoadAction?.Invoke();
        }

        private void BGLoader_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            Status2 = "A number of things have been Loaded.";
        }

        private void BGLoader_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker Worker = sender as BackgroundWorker;
            Status1 = "Loading Basic.";

            //More Housekeeping to be done once list is loaded.
            NDiceAdvanced = IntX.Pow(10, (uint)NDice);
            NSidesAdvanced = IntX.Pow(10, (uint)NSides);

            Status1 = "Finished Loading.";
        }
        #endregion
        #endregion

        #region HELPER FUNCTIONS
        #region BGWORKER

        #endregion
        #endregion

        #region PROPERTIES
        #region BGWORKERS
        private BackgroundWorker BGLoader;
        private BackgroundWorker BGWorker;
        private BackgroundWorker BGWorkerAdvanced;

        private Action PostLoadAction;
        private System.Timers.Timer BGTimer;
        private DateTime BGTimerTime;
        private bool BGTimerReport;
        #region BGWORKER
        private ulong _Coprimes;
        public ulong Coprimes
        {
            get
            {
                return _Coprimes;
            }
            set
            {
                _Coprimes = value;
                OnPropertyChanged("Coprimes");
            }
        }

        private ulong _Cofactors;
        public ulong Cofactors
        {
            get
            {
                return _Cofactors;
            }
            set
            {
                _Cofactors = value;
                OnPropertyChanged("Cofactors");
            }
        }

        private IntX _CoprimesAdvanced;
        public IntX CoprimesAdvanced
        {
            get
            {
                return _CoprimesAdvanced;
            }
            set
            {
                _CoprimesAdvanced = value;
                OnPropertyChanged("CoprimesAdvanced");
            }
        }

        private IntX _CofactorsAdvanced;
        public IntX CofactorsAdvanced
        {
            get
            {
                return _CofactorsAdvanced;
            }
            set
            {
                _CofactorsAdvanced = value;
                OnPropertyChanged("CofactorsAdvanced");
            }
        }

        private List<TrialRun> _L;
        public List<TrialRun> L
        {
            get
            {
                return _L;
            }
            set
            {
                _L = value;
                OnPropertyChanged("L");
            }
        }
        #endregion
        #endregion
        #region STATUSES
        private string _Status;
        public string Status
        {
            get
            {
                return _Status;
            }
            set
            {
                _Status = value;
                OnPropertyChanged("Status");
            }
        }

        private string _Status1;
        public string Status1
        {
            get
            {
                return _Status1;
            }
            set
            {
                _Status1 = value;
                OnPropertyChanged("Status1");
            }
        }

        private string _Status2;
        public string Status2
        {
            get
            {
                return _Status2;
            }
            set
            {
                _Status2 = value;
                OnPropertyChanged("Status2");
            }
        }

        private bool _Running = false;
        public bool Running
        {
            get
            {
                return _Running;
            }
            set
            {
                _Running = value;
                OnPropertyChanged("Running");
            }
        }

        private bool _Stopped = true;
        public bool Stopped
        {
            get
            {
                return _Stopped;
            }
            set
            {
                _Stopped = value;
                OnPropertyChanged("Stopped");
            }
        }
        #endregion
        #region INPUTS
        private ulong _NDice;
        public ulong NDice
        {
            get
            {
                return _NDice;
            }
            set
            {
                _NDice = value;
                Properties.Settings.Default.NDice = value;
                Properties.Settings.Default.Save();
                OnPropertyChanged("NDice");
            }
        }

        private ulong _NSides;
        public ulong NSides
        {
            get
            {
                return _NSides;
            }
            set
            {
                _NSides = value;
                Properties.Settings.Default.NSides = value;
                Properties.Settings.Default.Save();
                OnPropertyChanged("NSides");
            }
        }

        private ulong _NTrials;
        public ulong NTrials
        {
            get
            {
                return _NTrials;
            }
            set
            {
                _NTrials = value;
                Properties.Settings.Default.NTrials = value;
                Properties.Settings.Default.Save();
                OnPropertyChanged("NTrials");
            }
        }

        private ulong _NUpdate;
        public ulong NUpdate
        {
            get
            {
                return _NUpdate;
            }
            set
            {
                _NUpdate = value;
                Properties.Settings.Default.NUpdate = value;
                Properties.Settings.Default.Save();
                OnPropertyChanged("NUpdate");
            }
        }

        private double _NError;
        public double NError
        {
            get
            {
                return _NError;
            }
            set
            {
                _NError = value;
                Properties.Settings.Default.NError = value;
                Properties.Settings.Default.Save();
                OnPropertyChanged("NError");
            }
        }

        private IntX _NDiceAdvanced;
        public IntX NDiceAdvanced
        {
            get
            {
                return _NDiceAdvanced;
            }
            set
            {
                _NDiceAdvanced = value;
                OnPropertyChanged("NDiceAdvanced");
            }
        }

        private IntX _NSidesAdvanced;
        public IntX NSidesAdvanced
        {
            get
            {
                return _NSidesAdvanced;
            }
            set
            {
                _NSidesAdvanced = value;
                OnPropertyChanged("NSidesAdvanced");
            }
        }
        #endregion
        #region PANELS
        private UserControl _MainWindowContentControl;
        public UserControl MainWindowContentControl
        {
            get
            {
                return _MainWindowContentControl;
            }
            set
            {
                if (_MainWindowContentControl != value)
                {
                    _MainWindowContentControl = value;
                    OnPropertyChanged("MainWindowContentControl");
                }
            }
        }
        #endregion
        #region SETTINGS
        public string BGImage
        {
            get
            {
                if (Properties.Settings.Default.BackgroundImage == 0)
                {
                    return "Media/Backgrounds/Background" + Properties.Settings.Default.BackgroundImage + ".png";
                }
                return "Media/Backgrounds/Background" + Properties.Settings.Default.BackgroundImage + ".jpg";
            }
            set
            {
                OnPropertyChanged("BGImage");
            }
        }

        private double bgOpacity;
        public double BGOpacity
        {
            get
            {
                return bgOpacity;
            }
            set
            {
                bgOpacity = value;
                OnPropertyChanged("BGOpacity");
            }
        }
        private void SetBGOpacity()
        {
            switch (Properties.Settings.Default.BackgroundImage)
            {
                case 1:
                    BGOpacity = 0.1;
                    break;
                case 2:
                    BGOpacity = 0.1;
                    break;
                case 3:
                    BGOpacity = 0.3;
                    break;
                case 4:
                    BGOpacity = 0.6;
                    break;
                case 5:
                    BGOpacity = 0.2;
                    break;
                case 6:
                    BGOpacity = 0.6;
                    break;
                case 7:
                    BGOpacity = 0.2;
                    break;
                case 8:
                    BGOpacity = 0.2;
                    break;
                case 9:
                    BGOpacity = 0.1;
                    break;
                case 10:
                    BGOpacity = 0.5;
                    break;
                default:
                    BGOpacity = 0.5;
                    break;
            }
        }
        #endregion
        #region MISC
        private ProgramVersion _ProgramVersion = new ProgramVersion();
        public string ProgramVersion
        {
            get
            {
                return _ProgramVersion.Version;
            }
        }
        #endregion
        #endregion

        #region COMMANDS
        #region BUTTONS
        private ICommand _Start;
        public ICommand Start
        {
            get
            {
                if (_Start == null)
                {
                    _Start = new RelayCommand(StartEx, null);
                }
                return _Start;
            }
        }
        private void StartEx(object p)
        {
            Running = true;
            Stopped = false;

            //Account for users stop-starting - Clear any lists etc here before continuing.

            //This allows us to modify the app with other methods later on, whilst still using the same loader without modification.
            //PostLoadAction = new Action(() =>
            //    BGWorker.RunWorkerAsync()
            //    );

            //BGLoader.RunWorkerAsync();

            BGWorker.RunWorkerAsync();
        }


        private ICommand _StartAdvanced;
        public ICommand StartAdvanced
        {
            get
            {
                if (_StartAdvanced == null)
                {
                    _StartAdvanced = new RelayCommand(StartAdvancedEx, null);
                }
                return _StartAdvanced;
            }
        }
        private void StartAdvancedEx(object p)
        {
            Running = true;
            Stopped = false;

            PostLoadAction = new Action(() =>
                BGWorkerAdvanced.RunWorkerAsync()
                );

            BGLoader.RunWorkerAsync();
        }

        private ICommand _Stop;
        public ICommand Stop
        {
            get
            {
                if (_Stop == null)
                {
                    _Stop = new RelayCommand(StopEx, null);
                }
                return _Stop;
            }
        }
        private void StopEx(object p)
        {
            if (BGLoader.IsBusy)
            {
                BGLoader.CancelAsync();
            }
            if (BGWorker.IsBusy)
            {
                BGWorker.CancelAsync();
            }

            Stopped = true;
            Running = false;
        }
        #endregion
        #region MISC
        private ICommand _OpenPreferences;
        public ICommand OpenPreferences
        {
            get
            {
                if (_OpenPreferences == null)
                {
                    _OpenPreferences = new RelayCommand(OpenPreferencesEx, null);
                }
                return _OpenPreferences;
            }
        }
        private void OpenPreferencesEx(object p)
        {
            PreferencesWindow PrefWindow = new PreferencesWindow();
            PreferencesWindowViewModel PrefWindowViewModel = new PreferencesWindowViewModel();
            PrefWindow.DataContext = PrefWindowViewModel;
            PrefWindow.Owner = p as MainWindow;
            PrefWindow.ShowDialog();

            if (PrefWindowViewModel.Saved)
            {
                if (PrefWindowViewModel.BG0)
                {
                    Properties.Settings.Default.BackgroundImage = 0;
                }
                if (PrefWindowViewModel.BG1)
                {
                    Properties.Settings.Default.BackgroundImage = 1;
                }
                if (PrefWindowViewModel.BG2)
                {
                    Properties.Settings.Default.BackgroundImage = 2;
                }
                if (PrefWindowViewModel.BG3)
                {
                    Properties.Settings.Default.BackgroundImage = 3;
                }
                if (PrefWindowViewModel.BG4)
                {
                    Properties.Settings.Default.BackgroundImage = 4;
                }
                if (PrefWindowViewModel.BG5)
                {
                    Properties.Settings.Default.BackgroundImage = 5;
                }
                if (PrefWindowViewModel.BG6)
                {
                    Properties.Settings.Default.BackgroundImage = 6;
                }
                if (PrefWindowViewModel.BG7)
                {
                    Properties.Settings.Default.BackgroundImage = 7;
                }
                if (PrefWindowViewModel.BG8)
                {
                    Properties.Settings.Default.BackgroundImage = 8;
                }
                if (PrefWindowViewModel.BG9)
                {
                    Properties.Settings.Default.BackgroundImage = 9;
                }
                if (PrefWindowViewModel.BG10)
                {
                    Properties.Settings.Default.BackgroundImage = 10;
                }
                Properties.Settings.Default.Save();
                OnPropertyChanged("BGImage");
                SetBGOpacity();
            }
        }

        public void WindowClosing(CancelEventArgs e)
        {

        }

        private ICommand _ThemedDialog;
        public ICommand ThemedDialog
        {
            get
            {
                if (_ThemedDialog == null)
                {
                    _ThemedDialog = new RelayCommand(ThemedDialogEx, null);
                }
                return _ThemedDialog;
            }
        }
        private void ThemedDialogEx(object p)
        {
            DialogWindow DialogWindow = new DialogWindow("Title", "Message", "PRESS ME", null);
            DialogWindow.Owner = p as MainWindow;
            DialogWindow.ShowDialog();
        }
        #endregion
        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                if (handler.Target is CollectionView)
                {
                    ((CollectionView)handler.Target).Refresh();
                }
                else
                {
                    handler(this, new PropertyChangedEventArgs(propertyName));
                }
            }
        }

        #endregion
    }

    class MathHelper
    {
        public static ulong SqrtN(ulong N, ulong RootN = 1)
        {
            if (RootN <= 0)
                RootN = 1;
            while (!IsSquareRoot(N, RootN))
            {
                RootN += N / RootN;
                RootN /= 2;
            }

            return RootN;
        }

        public static IntX SqrtN(IntX N)
        {
            IntX RootN = 1;
            while (!IsSquareRoot(N, RootN))
            {
                RootN += N / RootN;
                RootN /= 2;
            }

            return RootN;
        }

        public static IntX SqrtN(IntX N, IntX RootN)
        {
            if (RootN <= 0)
                RootN = 1;
            while (!IsSquareRoot(N, RootN))
            {
                RootN += N / RootN;
                RootN /= 2;
            }

            return RootN;
        }

        private static bool IsSquareRoot(ulong N, ulong RootN)
        {
            ulong lowerBound = RootN * RootN;
            ulong upperBound = (RootN + 1) * (RootN + 1);
            return (N >= lowerBound && N < upperBound);
        }

        private static bool IsSquareRoot(IntX N, IntX RootN)
        {
            IntX lowerBound = RootN * RootN;
            IntX upperBound = (RootN + 1) * (RootN + 1);
            return (N >= lowerBound && N < upperBound);
        }

        public static ulong GCD(ulong a, ulong b)
        {
            while (a != 0 && b != 0)
            {
                if (a > b)
                    a %= b;
                else
                    b %= a;
            }

            return a == 0 ? b : a;
        }

        public static IntX GCD(IntX a, IntX b)
        {
            while (a != 0 && b != 0)
            {
                if (a > b)
                    a %= b;
                else
                    b %= a;
            }

            return a == 0 ? b : a;
        }
    }

    class TrialRun
    {
        public TrialRun()
        {

        }

        public TrialRun(ulong Coprimes, ulong Cofactors) : this()
        {
            this.Coprimes = Coprimes;
            this.Cofactors = Cofactors;
            Pi = Math.Sqrt(6d / ((double)Coprimes / ((double)Coprimes + (double)Cofactors)));
            Error = Math.Abs(Math.PI - Pi) / Math.PI * 100;
        }

        private ulong _Coprimes;
        public ulong Coprimes
        {
            get
            {
                return _Coprimes;
            }
            set
            {
                _Coprimes = value;
            }
        }

        private ulong _Cofactors;
        public ulong Cofactors
        {
            get
            {
                return _Cofactors;
            }
            set
            {
                _Cofactors = value;
            }
        }

        private double _Pi;
        public double Pi
        {
            get
            {
                return _Pi;
            }
            set
            {
                _Pi = value;
            }
        }

        private double _Error;
        public double Error
        {
            get
            {
                return _Error;
            }
            set
            {
                _Error = value;
            }
        }

        private IntX _CoprimesAdvanced;
        public IntX CoprimesAdvanced
        {
            get
            {
                return _CoprimesAdvanced;
            }
            set
            {
                _CoprimesAdvanced = value;
            }
        }

        private IntX _CofactorsAdvanced;
        public IntX CofactorsAdvanced
        {
            get
            {
                return _CofactorsAdvanced;
            }
            set
            {
                _CofactorsAdvanced = value;
            }
        }
    }
}
