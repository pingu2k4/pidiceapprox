# README #

Pi day 2017!

[VIDEO](https://www.youtube.com/watch?v=RZBhSi_PwHU&t=0s)

## WHERE DO I GET IT? ##
Download either the source code (for programmers) or the setup.exe (for those who want to play about and run the program) from the [downloads section](https://bitbucket.org/pingu2k4/pidiceapprox/downloads/).

### INSTRUCTIONS ###

* Enter the number of dice to roll. In the video, this was 500. Higher = more accurate.
* Enter the number of sides per dice. In the video, this was 120. Higher = more accurate.
* Enter the number of trials. In the video, this was 1. Increasing this performs the same thing over and over again, collecting all the results for an average.
* Enter the error%. This is any number (decimals allowed) between 0 and 100. This is the percentage chance that we incorrectly assign a coprime or cofactor when we roll 2 dice.
* Hit the start button! It will update you with its progress at the end of each trial, and once finished will display the average results, the best and worst result.

![1.png](https://bitbucket.org/repo/5qx9oap/images/3012541916-1.png)

### Who do I talk to? ###

* @Numberlists and @JagexPingu on Twitter.